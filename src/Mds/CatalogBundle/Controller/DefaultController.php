<?php

namespace Mds\ReferralBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package Mds\ReferralBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param Request $Request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $Request)
    {
        return $this->render('MdsCatalogBundle:Default:index.html.twig');
    }
}