<?php

namespace Mds\ReferralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Referral
 * @package Mds\ReferralBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(
 *  name="referral_info",
 *  indexes={
 *      @ORM\Index(name="ref_code_idx", columns={"ref_code"})
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class Referral
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="ref_code", type="string", length=6)
     * @var string
     */
    protected $refCode;

    /**
     * @ORM\Column(name="referer", type="string", length=255, nullable=true)
     * @var string
     */
    private $referer;

    /**
     * @ORM\Column(name="ip", type="string", length=255, nullable=true)
     * @var string
     */
    private $ip;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set refCode
     * @param string $refCode
     * @return Referral
     */
    public function setRefCode($refCode)
    {
        $this->refCode = $refCode;

        return $this;
    }

    /**
     * Get refCode
     * @return string
     */
    public function getRefCode()
    {
        return $this->refCode;
    }

    /**
     * Set referer
     * @param string $referer
     * @return Referral
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * Get referer
     * @return string
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Set ip
     * @param string $ip
     * @return Referral
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set createdAt
     * @ORM\PrePersist
     * @return Referral
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
