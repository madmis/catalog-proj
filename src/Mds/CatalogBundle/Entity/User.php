<?php

namespace Mds\ReferralBundle\Entity;

use FOS\UserBundle\Model\User as AbstractUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @package Mds\ReferralBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="ref_user")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends AbstractUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="first_name", type="string", length=100, nullable=true)
     * @var string
     */
    protected $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=100, nullable=true)
     * @var string
     */
    protected $lastName;

    /**
     * @ORM\Column(name="ref_code", type="string", length=6, unique=true)
     * @var string
     */
    protected $refCode;


    /**
     * @ORM\ManyToOne(targetEntity="Mds\ReferralBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="referral_id", referencedColumnName="id")
     * })
     * @var User
     */
    private $referral;

    /**
     * @ORM\Column(name="referer", type="string", length=255, nullable=true)
     * @var string
     */
    private $referer;

    /**
     * @ORM\Column(name="ip", type="string", length=255, nullable=true)
     * @var string
     */
    private $ip;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     * @var \DateTime
     */
    protected $createdAt;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set refCode
     * @param string $refCode
     * @return User
     */
    public function setRefCode($refCode = null) {
        $this->refCode = $refCode;

        return $this;
    }

    public static function generateRefCode($length = 6)
    {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($chars), 0, $length);
    }

    /**
     * Get refCode
     * @return string
     */
    public function getRefCode()
    {
        return $this->refCode;
    }

    /**
     * Set referral
     * @param User $Referral
     * @return User
     */
    public function setReferral(User $Referral)
    {
        $this->referral = $Referral;

        return $this;
    }

    /**
     * Get referral
     * @return User
     */
    public function getReferral()
    {
        return $this->referral;
    }

    /**
     * Set createdAt
     * @ORM\PrePersist
     * @return User
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        $this->username = $email;

        return $this;
    }

    /**
     * Set referer
     * @param string $referer
     * @return Referral
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * Get referer
     * @return string
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Set ip
     * @param string $ip
     * @return Referral
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }
}
