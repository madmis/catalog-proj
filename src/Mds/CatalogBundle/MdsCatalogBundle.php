<?php

namespace Mds\CatalogBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class MdsCatalogBundle
 * @package Mds\MdsCatalogBundle
 */
class MdsCatalogBundle extends Bundle
{
}