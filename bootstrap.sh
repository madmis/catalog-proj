#!/bin/bash

APP_ROOT_PATH="/vagrant"
DOMAIN="catalog.vagrant"
MYSQL_ROOT_PASS="root"
PROJECT_NAME="catalog"

locale-gen en_US.UTF-8
dpkg-reconfigure -f noninteractive tzdata

apt-get update && apt-get upgrade -y
apt-get install -y vim
apt-get update
# set default root password for mariadb (mysql)
debconf-set-selections <<< 'mysql-server mysql-server/root_password password '${MYSQL_ROOT_PASS}
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password '${MYSQL_ROOT_PASS}

apt-get install -y mysql-server nginx curl

# all posible version control systems for needs of the composer
apt-get install -y git

apt-get install -y php5-fpm php5-cli php5-dev php5-mongo php5-geoip php5-mcrypt php5-intl php5-curl
apt-get install -y php5-mysql php5-readline php5-tidy php-apc php-pear php5-json
apt-get install -y php5-xdebug

service php5-fpm restart

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

mysql -u root -p${MYSQL_ROOT_PASS} -e 'CREATE DATABASE IF NOT EXISTS `catalog`'

if [ ! -e ${APP_ROOT_PATH}/app/config/parameters.yml ]; then
    cat ${APP_ROOT_PATH}/app/config/parameters.yml.dist |sed "s/\(database_user:\).*\$/\\1 root/g"|sed "s/\(database_password:\).*\$/\\1 ${MYSQL_ROOT_PASS}/g">${APP_ROOT_PATH}/app/config/parameters.yml
fi
cd ${APP_ROOT_PATH}
/usr/local/bin/composer install -n
cd -

cp /vagrant/etc/php-fpm-pool.conf /etc/php5/fpm/pool.d/${PROJECT_NAME}.conf
service php5-fpm restart

# configure logrotate
cat ${APP_ROOT_PATH}/etc/logrotate.conf | sed "s%{PROJECT_PATH}%${APP_ROOT_PATH}%g" >/etc/logrotate.d/${PROJECT_NAME}.conf


cat ${APP_ROOT_PATH}/etc/nginx-host.conf|sed "s%{PROJECT_PATH}%${APP_ROOT_PATH}%g"|\
    sed "s%{DOMAIN}%${DOMAIN}%g" | sed "s%{UI_HOST}%${UI_HOST}%g"|sed "s%{FRONT_CONTROLLER}%app_dev%g">/etc/nginx/sites-available/${PROJECT_NAME}.conf
ln -s /etc/nginx/sites-available/${PROJECT_NAME}.conf /etc/nginx/sites-enabled/
service nginx restart

# fill the database
${APP_ROOT_PATH}/app/console doctrine:schema:create -e prod

# define address to user server for vagrant env.
echo "127.0.0.1 ${DOMAIN}" >> /etc/hosts

# configure xdebug
cat ${APP_ROOT_PATH}/etc/xdebug.ini >> /etc/php5/mods-available/xdebug.ini
service php5-fpm restart